public class Human {
    StringBuilder lastName;
    StringBuilder firstName;
    StringBuilder middleName;

    public Human(StringBuilder lastName, StringBuilder firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public Human(StringBuilder lastName, StringBuilder firstName, StringBuilder middleName) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
    }

    public String getFullName() {
        if(!(middleName ==null)) {
            return lastName.toString() +" "+ firstName.toString() +" "+ middleName.toString();
        }else{
            return lastName.toString() +" "+ firstName.toString();
        }
    }

    public String getShortName() {
        if(!(middleName ==null)) {
            return lastName.toString() +" "+ firstName.substring(0,1).toString() +". "+ middleName.substring(0,1).toString()+".";
        }else{
            return lastName.toString() +" "+ firstName.substring(0,1).toString()+".";
        }
    }
}
