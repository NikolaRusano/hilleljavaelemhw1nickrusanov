public class demoHuman {
    public static void main(String[] args) {
        Human h1 = new Human(new StringBuilder("Pitt"), new StringBuilder("Brad"));
        Human h2 = new Human(new StringBuilder("Geller"), new StringBuilder("Monica"), new StringBuilder("Velula"));

        System.out.println(h1.getFullName());
        System.out.println(h1.getShortName());

        System.out.println(h2.getFullName());
        System.out.println(h2.getShortName());

    }
}
